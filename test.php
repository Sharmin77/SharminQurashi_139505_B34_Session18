<?php
class BITM{
    public $chair;

    public function setChair($local_chair){
        $this->chair = $local_chair. "[set by parent]";
    }
    public  function show(){
        echo $this->chair."<br>";
    }
}//end of BITM class

class BITM_Lab402 extends BITM
{

    public function setChairFromChild($child_chair)
    {
        $child_chair = $child_chair . " [ set by child] ";

        parent::setChair($child_chair);

    }
}
$myParentObj = new BITM();
$myParentObj->setChair("I'm a chair");
$myParentObj->show();

$myChildObj = new BITM_Lab402();
$myChildObj->setChair("This is a Chair");
$myChildObj->show();


$myChildObj->setChairFromChild("Ami Ekti Chair");
echo $myChildObj->chair. "<br>";


