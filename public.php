<?php


class Person {

    private $name ;

    public function setName($localName)
    {

        $this->name = $localName;
    }

    public function getName()
    {
        return $this->name;
    }

}


class Actor extends Person{

}

class Student extends Person{

    public $studentID;

    public function setStudentID($studentID)
    {
        $this->studentID = $studentID;
    }

    public function studentInfo(){

        echo $this->studentID."<br>";


        parent::setName("Faisal Korim");
        echo parent::getName()."<br>";

    }
}


class food{

    public function testMethod(){
        $objPerson = new Person();
        $odjStudent = new Student();


        $objPerson->setName("Food Person");
        echo $objPerson->getName()."<br>";

        $odjStudent->setName("Food Student");
        echo $odjStudent->getName()."<br>";


        $odjStudent->studentInfo();

    }




}


$objectOfaPerson = new Person();
$objectOfaPerson->setName("Salman Khan");
$result = $objectOfaPerson->getName();
echo $result;

$objectOfaStudent = new Student();
$objectOfaStudent->setStudentID("SEIP98234");
$objectOfaStudent->studentInfo();


$objFood = new Food();
$objFood->testMethod();